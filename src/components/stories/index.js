import React from 'react';

import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import H1 from '../h1/styled';
import Button from '../buttons/styled';


storiesOf('H1', module)
  .add('Centered, Fade In', () => <H1>Centered Big Header</H1>)

storiesOf('Button', module)
  .add('Standard', () => <Button onClick={action('clicked')}>Button</Button>)
  .add('Primary', () => <Button primary onClick={action('clicked')}>Primary</Button>)
