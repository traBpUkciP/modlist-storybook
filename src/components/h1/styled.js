import styled from 'styled-components'
import { keyframes } from 'styled-components'

const H1 = styled.h1`
    font-size: 1.5em;
    text-align: center;
    color: palevioletred;
    animation: ${keyframes`from { opacity: 0; }`} 1s both;
`

export default H1
